# Home24 Bandsintown Challenge

[LIVE DEMO !!!](https://www.bandsintown.michaelgerullis.de/)

This project was generated with:



- [Angular CLI](https://github.com/angular/angular-cli) version 7.1.0.
- [Bulma](https://bulma.io)
- [@ngx/leaflet](https://github.com/Asymmetrik/ngx-leaflet)
- Love

Angular-Cli command which created this project:

`ng new home24-bandsintown --commit=false --prefix=home24 --routing=true --style=scss`

## Documentation

Anything not explained here should be explained in form of jsDoc

## Continuous Integration

- This project is delivered by [Gitlab CI/CD](https://docs.gitlab.com/ee/ci/)
- see the [gitlab-ci.yml](https://gitlab.com/mchl18/home24-bandsintown/blob/master/.gitlab-ci.yml)

## Development server

Run `yarn start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `yarn build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `yarn test` to execute the unit tests via [Karma](https://karma-runner.github.io).
