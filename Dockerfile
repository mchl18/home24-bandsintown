FROM node:latest

WORKDIR /usr/app

RUN apt-get update && apt-get install -y \
  apt-utils \
  apt-transport-https \
  ca-certificates \
  curl \
  gnupg \
  --no-install-recommends \
  && curl -sSL https://dl.google.com/linux/linux_signing_key.pub | apt-key add - \
  && echo "deb https://dl.google.com/linux/chrome/deb/ stable main" > /etc/apt/sources.list.d/google-chrome.list \
  && apt-get update && apt-get install -y \
  google-chrome-beta \
  fontconfig \
  fonts-ipafont-gothic \
  fonts-wqy-zenhei \
  fonts-thai-tlwg \
  fonts-kacst \
  fonts-noto \
  ttf-freefont \
  --no-install-recommends \
  && rm -rf /var/lib/apt/lists/*

RUN export CHROME_BIN=/usr/bin/google-chrome

RUN git clone https://gitlab.com/mchl18/home24-bandsintown.git /usr/app

RUN npm install
