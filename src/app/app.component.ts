import { Component } from '@angular/core';

@Component({
  selector: 'home24-root',
  template: '<router-outlet></router-outlet>',
})
export class AppComponent {}
