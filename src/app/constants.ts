const ARTIST_NULL = 'ARTIST_NULL';
const SESSION_STORAGE_TAG = 'HOME24_BANDSINTOWN_LAST_ARTIST';

export { ARTIST_NULL, SESSION_STORAGE_TAG };
