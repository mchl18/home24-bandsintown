import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';

import { memoize } from 'decko';
import { Observable } from 'rxjs';

import { ARTIST_NULL, SESSION_STORAGE_TAG } from '@app/constants';
import { ArtistData } from '@app/models/artist-data';
import { BandsintownService } from '@app/services/bandsintown.service';
import { SessionStorageService } from '@app/services/session-storage.service';

@Component({
  selector: 'home24-artist-page',
  templateUrl: './artist-page.component.html',
  styleUrls: ['./artist-page.component.scss'],
})
export class ArtistPageComponent implements OnInit {
  public artistName = 'Ninze';
  public artist: ArtistData;

  constructor(
    private activatedRoute: ActivatedRoute,
    private location: Location,
    private bandsintownService: BandsintownService,
    private titleService: Title,
    private sessionStorageService: SessionStorageService,
  ) { }

  public ngOnInit() {
    this._getArtistFromRouteParams();
  }

  private _getArtistFromRouteParams() {
    this.activatedRoute.queryParams.subscribe(
      (res) => {
        const urlArtist = Object.keys(res)[0];
        const storageArtist = this.sessionStorageService.get(SESSION_STORAGE_TAG);
        if (urlArtist) {
          this.artistName = urlArtist;
        } else {
          this.artistName = storageArtist;
        }
        this.location.replaceState('', this.artistName);
      },
    );
  }

  @memoize
  public getArtist(artistName: string): Observable<ArtistData | any> {
    return this.bandsintownService.getArtist(artistName);
  }

  public setArtist(artistName = '') {
    if (artistName === ARTIST_NULL) {
      this.artistName = null;
      this.location.replaceState('', '');
      return;
    }
    this.artistName = artistName.trim();
    this.location.replaceState('', this.artistName);
    this.titleService.setTitle(`events for: ${artistName}`);
    this.sessionStorageService.set(SESSION_STORAGE_TAG, artistName);
  }
}
