import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Title } from '@angular/platform-browser';

import { createTestComponentFactory, Spectator } from '@netbasal/spectator';

import { ARTIST_NULL, SESSION_STORAGE_TAG } from '@app/constants';
import { BandsintownService } from '@app/services/bandsintown.service';
import { SessionStorageService } from '@app/services/session-storage.service';

import { ArtistPageComponent } from './artist-page.component';

describe('ArtistPageComponent', () => {
  let spectator: Spectator<ArtistPageComponent>;
  const createComponent = createTestComponentFactory<ArtistPageComponent>({
    component: ArtistPageComponent,
    componentProviders: [BandsintownService, SessionStorageService],
    imports: [HttpClientTestingModule],
    shallow: true,
  });

  beforeEach(() => {
    /**
     * This may look brutal
     * But in case of http://localhost:9876/ ...
     * ... I really don't care
     */
    sessionStorage.clear();
  });

  it('should create', () => {
    spectator = createComponent();
    expect(spectator.component).toBeTruthy();
  });

  it('should call the bandsintown service', () => {
    /**
     * Set an init value
     */
    spectator = createComponent({
      artistName: 'Ninze',
    });
    const spy = spyOn(spectator.component, 'getArtist');
    /**
     * Run template logic
     */
    spectator.detectChanges();

    expect(spy).toHaveBeenCalled();
    expect(spy).toHaveBeenCalledWith('Ninze');
  });

  describe('ngOnInit', () => {
    beforeEach(() => {
      sessionStorage.clear();
    });

    it('should use use the last searched value from session storage', () => {
      /**
       * This init val should be overwritten
       */
      spectator = createComponent({
        artistName: 'foo',
      });

      /**
       * Simulate last search and init
       */
      spectator.component.setArtist('Matanza');
      spectator.component.ngOnInit();
      expect(spectator.component.artistName).toEqual('Matanza');
    });
  });

  describe('setArtist', () => {
    beforeEach(() => {
      sessionStorage.clear();
      spectator = createComponent();
    });

    it('should set the artist', () => {
      spectator.component.setArtist('Okaxy');
      expect(spectator.component.artistName).toEqual('Okaxy');
    });

    it('should reset the artist if none was found', () => {
      spectator.component.setArtist(ARTIST_NULL);
      expect(spectator.component.artistName).toBeNull();
    });

    it('should set the title of the document', () => {
      const titleMock = spectator.get<Title>(Title, true);
      spyOn(titleMock, 'setTitle');
      spectator.component.setArtist('Okaxy');
      expect(titleMock.setTitle).toHaveBeenCalledWith(`events for: Okaxy`);
    });

    it('should call session storage service to persist last searched artist', () => {
      const service = spectator.get(SessionStorageService, true);
      spyOn(service, 'set');
      spectator.component.setArtist('Okaxy');
      expect(service.set).toHaveBeenCalledWith(SESSION_STORAGE_TAG, 'Okaxy');
    });
  });
});
