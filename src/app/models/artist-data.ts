/**
 * https://app.swaggerhub.com/apis/Bandsintown/PublicAPI/3.0.0
 * ArtistData
 */
export interface ArtistData {
  name: string;
  url: string;
  id: number;
  image_url: string;
  thumb_url: string;
  facebook_page_url: string;
  mbid: string;
  tracker_count: number;
  upcoming_event_count: number;
}
