interface VenueData {
  name: string;
  latitude: string;
  longitude: string;
  city: string;
  region: string;
  country: string;
}

interface OfferData {
  type: string;
  url: string;
  status: string;
}

interface ArtistEvent {
  id: string;
  artist_id: string;
  url: string;
  on_sale_datetime?: string;
  datetime: string;
  venue: VenueData;
  offers?: OfferData[];
  lineup?: string[];
}

export { VenueData, OfferData, ArtistEvent };
