import { createService } from '@netbasal/spectator';
import { SessionStorageService } from '../session-storage.service';

describe('SessionStorageService Without Mock', () => {
  const spectator = createService(SessionStorageService);

  it('should create', () => {
    expect(spectator.service).toBeTruthy();
  });

  describe('set', () => {
    beforeEach(() => {
      // Just to be sure since we are testing a local dev domain.
      sessionStorage.clear();
    });

    it('should set and get the item accordingly', () => {
      const testvar = { name: 'morefox' };
      const name = 'JASMINE_TEST';
      spectator.service.set(name, testvar);
      const fromStorage = spectator.service.get(name);
      expect(fromStorage).toEqual(testvar);
    });
  });
});
