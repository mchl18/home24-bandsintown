import { createService } from '@netbasal/spectator';
import { of } from 'rxjs';
import { BandsintownService, reqOpts } from '../bandsintown.service';
import { RequestService } from '../request.service';

describe('BandsintownService Without Mock', () => {
  // const spectator = createService(BandsintownService);
  const spectator = createService({
    service: BandsintownService,
    mocks: [RequestService],
  });

  it('should be create', () => {
    expect(spectator.service).toBeTruthy();
  });

  describe('getArtist', () => {
    it('should return an empty observable if no name is given', (done) => {
      expect(
        spectator.service.getArtist('').subscribe((res) => {
          expect(res).toBeNull();
          done();
        }),
      );
    });

    it('should call the request service for an artist', () => {
      const mockService = spectator.get<RequestService>(RequestService);
      const mockRequest = 'https://rest.bandsintown.com/artists/Kendrick';
      mockService.get.and.returnValue(of(''));
      spectator.service.getArtist('Kendrick').subscribe(() => { });
      expect(mockService.get).toHaveBeenCalledTimes(1);
      expect(mockService.get).toHaveBeenCalledWith(mockRequest, reqOpts);
    });

    it('should call the request service for an artists events', () => {
      const mockService = spectator.get<RequestService>(RequestService);
      const mockRequest =
        'https://rest.bandsintown.com/artists/Kendrick/events';
      mockService.get.and.returnValue(of(''));
      spectator.service.getArtistEvents('Kendrick').subscribe(() => { });
      expect(mockService.get).toHaveBeenCalledTimes(1);
      expect(mockService.get).toHaveBeenCalledWith(mockRequest, reqOpts);
    });
  });
});
