import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';

import { Observable } from 'rxjs';
import { RequestService } from '../request.service';

describe('RequestService', () => {
  let service: RequestService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });

    service = TestBed.get(RequestService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('returns observables', () => {
    expect(service.get('foo')).toEqual(jasmine.any(Observable));
    expect(service.get('post')).toEqual(jasmine.any(Observable));
  });
});
