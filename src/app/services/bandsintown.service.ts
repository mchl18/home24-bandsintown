import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { filter } from 'rxjs/operators';

import { ArtistData, ArtistEvent } from '@app/models';

import { RequestService } from './request.service';

export const reqOpts = {
  app_id: 'helloHome24',
};

/**
 * Service which connects to bandsintown API
 */
@Injectable({
  providedIn: 'root',
})
export class BandsintownService {
  constructor(private requestService: RequestService) {}

  /**
   * Bandsintown API:
   * https://app.swaggerhub.com/apis/Bandsintown/PublicAPI/3.0.0
   */
  private baseUrl = 'https://rest.bandsintown.com';
  private endpoints = {
    getArtist: (name: string) => `${this.baseUrl}/artists/${name}`,
    getArtistEvents: (name: string) => `${this.baseUrl}/artists/${name}/events`,
  };

  public getArtist(artist: string): Observable<ArtistData | null> {
    if (!artist) {
      return of(null);
    }
    return this.requestService
      .get(this.endpoints.getArtist(artist), reqOpts)
      .pipe(
        /**
         * Sometimes an empty string is returned.
         * Other times it is an actual error message.
         */
        filter(x => !!x),
        filter(x => !(x.error && x.error === 'Not Found')),
      );
  }

  public getArtistEvents(artist: string): Observable<ArtistEvent[]> {
    return this.requestService.get(
      this.endpoints.getArtistEvents(artist),
      reqOpts,
    );
  }
}
