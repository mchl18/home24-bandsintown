import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class SessionStorageService {
  constructor() {}

  public get(name: string) {
    return JSON.parse(sessionStorage.getItem(name));
  }

  public set(name: string, value: any) {
    sessionStorage.setItem(name, JSON.stringify(value));
  }
}
