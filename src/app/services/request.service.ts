import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { memoize } from 'decko';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class RequestService {
  constructor(private http: HttpClient) {}

  @memoize
  public get(url: string, opts?: any): Observable<any> {
    const params = this.getParams(opts);
    return this.http.get(url, { params });
  }

  public post(url: string, body: any, opts?: any): Observable<any> {
    return this.http.post(url, body, opts);
  }

  private getParams(params: any): null | HttpParams {
    if (!params) {
      return null;
    }
    if (params instanceof HttpParams) {
      return params;
    }
    return new HttpParams({ fromObject: params });
  }
}
