import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ArtistPageComponent } from './routes';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'app' },
  {
    path: '',
    component: ArtistPageComponent,
  },
  { path: '**', redirectTo: '' },
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
