import { createTestComponentFactory, Spectator } from '@netbasal/spectator';
import { AppComponent } from './app.component';

describe('AppComponentComponent', () => {
  let spectator: Spectator<AppComponent>;
  const createComponent = createTestComponentFactory<AppComponent>({
    component: AppComponent,
    shallow: true,
  });

  it('should create', () => {
    spectator = createComponent();
    expect(spectator.component).toBeTruthy();
  });
});
