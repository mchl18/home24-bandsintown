import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { ArtistPageComponent } from './routes';

import {
  ArtistEventsModule,
  ArtistInfoModule,
  SearchBarModule,
} from './components';

@NgModule({
  declarations: [
    AppComponent,

    /**
     * Routes
     */
    ArtistPageComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,

    /**
     * Http
     */
    HttpClientModule,

    /**
     * Internal modules
     */
    ArtistInfoModule,
    ArtistEventsModule,
    SearchBarModule,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
