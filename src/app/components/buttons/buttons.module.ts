import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { EventBtnComponent } from './event-btn';
import { TicketBtnComponent } from './ticket-btn/ticket-btn.component';

@NgModule({
  imports: [CommonModule],
  declarations: [EventBtnComponent, TicketBtnComponent],
  exports: [EventBtnComponent, TicketBtnComponent],
})
export class ButtonModule {}
