import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'home24-event-btn',
  template: `
    <a
      class="button is-primary event-button"
      *ngIf="url"
      [href]="url"
      target="_blank"
    >
      <span class="icon">
        <img class="fist" src="assets/img/bitFist-white.svg" alt="bit fist" />
      </span>
      <span>View Event</span>
    </a>
  `,
  styleUrls: ['../buttons.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EventBtnComponent {
  @Input() url: string;
}
