import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { OfferData } from '@app/models';

@Component({
  selector: 'home24-ticket-btn',
  template: `
    <a
      class="ticket-link button is-link ticket-button"
      *ngFor="let ticket of getAvailableTickets(offers)"
      [href]="ticket.url"
      target="_blank"
    >
      <span class="icon"> <i class="icon fa fa-ticket-alt"></i> </span>
      <span class="">Get Tickets!</span>
    </a>
  `,
  styleUrls: ['../buttons.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TicketBtnComponent {
  @Input() offers: OfferData[];

  public getAvailableTickets(offers: OfferData[] = []) {
    if (!offers.length) {
      return;
    }
    return offers.filter(o => o.status === 'available' && o.type === 'Tickets');
  }
}
