import { Component } from '@angular/core';
import {
  createHostComponentFactory,
  SpectatorWithHost,
} from '@netbasal/spectator';

import { createMockOffer, mockOffers } from './mock';
import { TicketBtnComponent } from './ticket-btn.component';

@Component({ selector: 'home24-host', template: '' })
class CustomHostComponent {
  public offers = mockOffers;
}

describe('TicketBtnComponent', () => {
  let host: SpectatorWithHost<TicketBtnComponent>;
  const createHost = createHostComponentFactory({
    component: TicketBtnComponent,
    host: CustomHostComponent,
  });

  it('should create', () => {
    host = createHost(`<home24-ticket-btn></home24-ticket-btn>`);
    expect(host.component).toBeTruthy();
  });

  it('should pass offers to the component', () => {
    host = createHost(
      `<home24-ticket-btn [offers]="offers"></home24-ticket-btn>`,
    );
    expect(host.component.offers).toEqual(mockOffers);
  });

  it('should render two buttons', () => {
    host = createHost(
      `<home24-ticket-btn [offers]="offers"></home24-ticket-btn>`,
    );
    expect(host.component.offers).toEqual(mockOffers);
    host.detectChanges();
    const btns = host.queryAll('.ticket-button');
    expect(btns.length).toBe(2);
  });

  it('should return only available tickets', () => {
    host = createHost(`<home24-ticket-btn></home24-ticket-btn>`);
    const mixedOffers = [
      createMockOffer('available'),
      createMockOffer('available'),
      createMockOffer('available'),
      createMockOffer('not_available'),
      createMockOffer('not_available'),
    ];

    expect(host.component.getAvailableTickets(mixedOffers).length).toBe(3);
  });
});
