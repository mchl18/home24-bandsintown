type Availability = 'available' | 'not_available';

/* tslint:disable */
const createMockOffer = (status: Availability) => ({
  status,
  type: 'Tickets',
  url:
    'https://www.bandsintown.com/t/1012332043?app_id=he…ium=api&utm_source=public_api&utm_campaign=ticket',
});

const mockOffers = [createMockOffer('available'), createMockOffer('available')];

export { Availability, createMockOffer, mockOffers };
