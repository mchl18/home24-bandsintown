import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ArtistInfoComponent } from './artist-info.component';

@NgModule({
  imports: [CommonModule],
  declarations: [ArtistInfoComponent],
  exports: [ArtistInfoComponent],
})
export class ArtistInfoModule {}
