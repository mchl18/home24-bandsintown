const mockArtist = {
  facebook_page_url: '',
  id: '3541597',
  image_url: 'https://s3.amazonaws.com/bit-photos/artistLarge.jpg',
  mbid: '',
  name: 'Ninze',
  thumb_url: 'https://s3.amazonaws.com/bit-photos/artistThumb.jpg',
  tracker_count: 1430,
  upcoming_event_count: 3,
  url: 'https://www.bandsintown.com/a/3541597?came_from=267&app_id=helloHome24',
} as any;

export { mockArtist };
