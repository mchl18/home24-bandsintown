import { createHostComponentFactory, Spectator } from '@netbasal/spectator';

import { Component } from '@angular/core';
import { ArtistInfoComponent } from './artist-info.component';
import { mockArtist } from './mock';

@Component({ selector: 'home24-host', template: '' })
class CustomHostComponent {
  artist = mockArtist;
}

describe('ArtistInfoComponent', () => {
  let host: Spectator<ArtistInfoComponent>;
  const createHost = createHostComponentFactory(ArtistInfoComponent);
  const createCustomHost = createHostComponentFactory({
    component: ArtistInfoComponent,
    host: CustomHostComponent,
  });

  it('should create', () => {
    host = createHost(`
      <home24-artist-info></home24-artist-info>
    `);
    expect(host.component).toBeTruthy();
  });

  it('should create', () => {
    host = createCustomHost(`
      <home24-artist-info [artist]="artist"></home24-artist-info>
    `);
    expect(host.component.artist).toEqual(mockArtist);
  });
});
