import { Component, Input } from '@angular/core';
import { ArtistData } from '@app/models';

/**
 * Component which displays artists name, picture and if available, facebook link
 */
@Component({
  selector: 'home24-artist-info',
  templateUrl: './artist-info.component.html',
  styleUrls: ['./artist-info.component.scss'],
})
export class ArtistInfoComponent {
  @Input() artist: ArtistData;
}
