import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SearchBarComponent } from './search-bar.component';

@NgModule({
  imports: [
    CommonModule,

    /**
     * Forms
     */
    FormsModule,
    ReactiveFormsModule,
  ],
  declarations: [SearchBarComponent],
  exports: [SearchBarComponent],
})
export class SearchBarModule {}
