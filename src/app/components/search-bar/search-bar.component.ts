import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { FormControl } from '@angular/forms';

import { debounce } from 'decko';

import { ARTIST_NULL } from '@app/constants';

@Component({
  selector: 'home24-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.scss'],
})
export class SearchBarComponent implements OnInit {
  public searchControl = new FormControl();
  @Input() initVal: string;
  @Output() valueChange = new EventEmitter<string>();
  @ViewChild('searchInput', { static: true }) searchInput: ElementRef;

  public ngOnInit(): void {
    if (this.initVal) {
      this.searchControl.patchValue(this.initVal);
    }
    this.searchInput.nativeElement.focus();
  }

  @debounce(500)
  public search(value = ''): void {
    this.emitValue(value);
  }

  private emitValue(val: string): void {
    if (val) {
      this.valueChange.emit(val);
    } else {
      this.valueChange.emit(ARTIST_NULL);
    }
  }
}
