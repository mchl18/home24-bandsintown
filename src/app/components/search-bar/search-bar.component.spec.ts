import { createTestComponentFactory, Spectator } from '@netbasal/spectator';
import { SearchBarComponent } from './search-bar.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

describe('SearchBarComponent', () => {
  let spectator: Spectator<SearchBarComponent>;
  const createComponent = createTestComponentFactory<SearchBarComponent>({
    component: SearchBarComponent,
    imports: [FormsModule, ReactiveFormsModule],
  });

  it('should create', () => {
    spectator = createComponent();
    expect(spectator.component).toBeTruthy();
  });
});
