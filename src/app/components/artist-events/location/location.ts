import * as L from 'leaflet';

const DEFAULT_LAT = '52.322';
const DEFAULT_LNG = '13.25385';

function parseLatLng(
  latitude = DEFAULT_LAT,
  longitude = DEFAULT_LNG,
): [number, number] {
  return [parseFloat(latitude), parseFloat(longitude)];
}

function getBaseOptionsFromLatLng(latLng: L.LatLng) {
  return {
    layers: [
      L.tileLayer('http://{s}.tiles.wmflabs.org/bw-mapnik/{z}/{x}/{y}.png', {
        maxZoom: 18,
      }),
      L.marker(latLng, {
        icon: L.divIcon({
          html: '<i class="fa fa-map-pin map-pin"></i>',
        }),
      }),
    ],
    center: latLng,
    zoom: 12,
    zoomControl: false,
    scrollWheelZoom: false,
  };
}

export { getBaseOptionsFromLatLng, parseLatLng };
