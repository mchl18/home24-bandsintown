import { LocationComponent } from './location.component';

import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { createTestComponentFactory, Spectator } from '@netbasal/spectator';

/**
 * An event is required for the component to show
 */
const mockEvent = {
  event: {
    venue: {
      name: 'foo',
      latitude: '42',
      longitude: '237',
    },
    datetime: '2018-12-04T00:39:49+0000',
  },
} as any;

describe('LocationComponent', () => {
  let spectator: Spectator<LocationComponent>;
  const createComponent = createTestComponentFactory<LocationComponent>({
    component: LocationComponent,
    imports: [LeafletModule],
    shallow: true,
  });

  beforeEach(() => {
    spectator = createComponent(mockEvent);
  });

  it('should create', () => {
    expect(spectator.component).toBeTruthy();
  });

  it('should show the name if event objectt is iben', () => {
    spectator.detectChanges();
    expect(spectator.query('.event-name').textContent).toEqual('foo');
  });

  it('should show the mini-map', () => {
    spectator.detectChanges();
    expect(spectator.query('.mini-map')).toExist();
  });

  it('should render the date correctly', () => {
    spectator.detectChanges();
    expect(spectator.query('.event-date').textContent).toEqual('04.12.2018');
  });

  describe('getLeafletOptsFromEvent', () => {
    it('should get opts which center on given lat/lng', () => {
      spectator = createComponent(mockEvent);
      const opts = spectator.component.getLeafletOptsFromEvent();
      expect(opts.center).toEqual({ lat: 42, lng: 237 });
    });
  });
});
