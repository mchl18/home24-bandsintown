import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

import * as L from 'leaflet';

import { ArtistEvent } from '@app/models';

import { getBaseOptionsFromLatLng, parseLatLng } from './location';

/**
 * Component which displays a mini-map preview of the events location
 */
@Component({
  selector: 'home24-location',
  templateUrl: './location.component.html',
  styleUrls: ['./location.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LocationComponent {
  @Input() event: ArtistEvent;

  public getLeafletOptsFromEvent(): L.MapOptions {
    const { latitude, longitude } = this.event.venue;
    if (this.event.venue) {
      const parsedLatLng = parseLatLng(latitude, longitude);
      const latLng = L.latLng(parsedLatLng);
      return getBaseOptionsFromLatLng(latLng);
    }
  }
}
