import { ArtistEventsComponent } from './artist-events.component';

import { createTestComponentFactory, Spectator } from '@netbasal/spectator';
import { of } from 'rxjs';

import { BandsintownService } from '@app/services/bandsintown.service';

import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ButtonModule } from '../buttons/buttons.module';

describe('ArtistEventsComponent', () => {
  let spectator: Spectator<ArtistEventsComponent>;
  const createComponent = createTestComponentFactory<ArtistEventsComponent>({
    component: ArtistEventsComponent,
    componentProviders: [BandsintownService],
    imports: [ButtonModule, HttpClientTestingModule],
    shallow: true,
  });

  it('should create', () => {
    spectator = createComponent();
    expect(spectator.component).toBeTruthy();
  });

  describe('getBandEvents', () => {
    it('should call the bandsintown service', () => {
      spectator = createComponent();
      const service = spectator.get<BandsintownService>(
        BandsintownService,
        true,
      );
      spyOn(service, 'getArtistEvents').and.returnValue(of({}));
      spectator.component.getBandEvents('Kendrick Lamar');
      expect(service.getArtistEvents).toHaveBeenCalledTimes(1);
      expect(service.getArtistEvents).toHaveBeenCalledWith('Kendrick Lamar');
    });
  });
});
