import {
  createHostComponentFactory,
  SpectatorWithHost,
} from '@netbasal/spectator';
import { LineupContainerComponent } from './lineup-container.component';

describe('LineupContainerComponent', () => {
  let host: SpectatorWithHost<LineupContainerComponent>;
  const createHost = createHostComponentFactory(LineupContainerComponent);

  it('should create', () => {
    host = createHost(`<home24-lineup-container></home24-lineup-container>`);
    expect(host.component).toBeTruthy();
  });

  describe('check if @Inputs are being passed', () => {
    beforeEach(() => {
      /**
       * Create a host, pass test values inline
       */
      host = createHost(`
        <home24-lineup-container [lineup]="['foo', 'bar']">
        </home24-lineup-container>
    `);
    });

    it('should create a list if lineup is given', () => {
      const lineupList = host.query('.lineup');
      expect(lineupList).toBeTruthy();
    });

    it('should pass and create two items', () => {
      const lineupList = host.query('.lineup');
      expect(lineupList.children.length).toEqual(2);
    });

    it('should create the items with the right text-content', () => {
      const lineupList = host.query('.lineup').children;
      expect(lineupList[0].textContent).toEqual('foo');
      expect(lineupList[1].textContent).toEqual('bar');
    });
  });
});
