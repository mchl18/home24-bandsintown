import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

/**
 * Component which displays a lineup which is a set of artists
 */
@Component({
  selector: 'home24-lineup-container',
  templateUrl: './lineup-container.component.html',
  styleUrls: ['./lineup-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LineupContainerComponent {
  @Input() lineup: string[];
}
