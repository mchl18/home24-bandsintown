import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

import { memoize } from 'decko';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { ArtistEvent } from '@app/models';
import { BandsintownService } from '@app/services';

/**
 * Component which displays a set of cards for each upcoming event of an artist.
 * Displays the events location, date and gives the possiblity to view the event
 * on bandsintwon.com or get tickets, if available
 */
@Component({
  selector: 'home24-artist-events',
  templateUrl: './artist-events.component.html',
  styleUrls: ['./artist-events.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ArtistEventsComponent {
  constructor(private bandsintownService: BandsintownService) {}

  @Input() artistName: string;

  /**
   * memoize: needed for preventing change detection to call over and over again
   */
  @memoize
  public getBandEvents(artistName: string): Observable<ArtistEvent[]> {
    return this.bandsintownService
      .getArtistEvents(artistName)
      .pipe(catchError(() => of([])));
  }
}
