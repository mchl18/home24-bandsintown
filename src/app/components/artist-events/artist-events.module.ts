import { NgModule } from '@angular/core';

import { CommonModule } from '@angular/common';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { ArtistEventsComponent } from './artist-events.component';
import { LineupContainerComponent } from './lineup-container/lineup-container.component';
import { LocationComponent } from './location/location.component';

import { ButtonModule } from '../buttons';

@NgModule({
  imports: [CommonModule, ButtonModule, LeafletModule.forRoot()],
  declarations: [
    ArtistEventsComponent,
    LineupContainerComponent,
    LocationComponent,
  ],
  exports: [ArtistEventsComponent],
})
export class ArtistEventsModule {}
